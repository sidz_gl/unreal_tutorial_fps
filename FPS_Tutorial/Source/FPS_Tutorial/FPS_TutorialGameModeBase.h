// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "FPS_TutorialGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class FPS_TUTORIAL_API AFPS_TutorialGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
